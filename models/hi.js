const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const HiSchema = new Schema({
    email: {
        type: String,
        minlength: 5,
        required: true,
        lowercase: true
    },
    from: {
        type: String,
        minlength: 5,
        required: true,
    },
    to: {
        type: String,
        minlength: 5,
        required: true,
    },
    date: {
        type: Date,
        required: true,
    }
});

const Hi = mongoose.model('hi', HiSchema);

module.exports = Hi;
