const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');
const express = require('express');

const app = express();
const config = require('./config/database');
const routes = require('./routes/routes');
const port = process.env.PORT || 8080; // Allows heroku to set port

mongoose.Promise = global.Promise;
mongoose.connect(config.uri, { useMongoClient: true }, (err) => {
  if(err)
    console.log('Could not connect ', err);
  else
    console.log('Connected to database.');
});

app.use(cors({ origin: 'http://localhost:4200' }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
routes(app);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

app.listen(port, () => {
  console.log('Express started on port ' + port + '.');
});
