import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

  // domain = 'http://localhost:8080';
  domain = '';
  authToken;
  options;

  constructor(private http: Http) { }

  createAuthenticationHeaders() {
    this.loadToken();
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
        'authorization': this.authToken
      })
    });
  }

  loadToken() {
    this.authToken = localStorage.getItem('token');
  }

  register(user) {
    return this.http.post(this.domain + '/register', user).map(res => res.json());
  }

  login(user) {
    return this.http.post(this.domain + '/login', user).map(res => res.json());
  }

  logout() {
    this.authToken = null;
    localStorage.clear();
  }

  storeUserData(token) {
    localStorage.setItem('token', token);
  }

  loggedIn() {
    return tokenNotExpired();
  }

  sayHi(to) {
    this.createAuthenticationHeaders();
    return this.http.post(this.domain + '/hi', to, this.options).map(res => res.json());
  }

  retrieveHis() {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + '/hi', this.options).map(res => res.json());
  }

  getAllUsers() {
    this.createAuthenticationHeaders();
    return this.http.get(this.domain + '/users', this.options).map(res => res.json());
  }

}
