import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  his;
  users;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.getHis();
    this.authService.getAllUsers().subscribe(data => {
      this.users = data.users;
    });
  }

  sayHi(username) {
    const to = {
      username: username
    };
    this.authService.sayHi(to).subscribe();
    this.getHis();
  }

  getHis() {
    this.authService.retrieveHis().subscribe(data => {
      this.his = data.his;
    });
  }

}
