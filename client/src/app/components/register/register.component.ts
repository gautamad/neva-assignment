import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form;
  messageClass;
  message;
  emailValid;
  emailMessage;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])],
      username: '',
      password: '',
      confirm: ''
    }, { validator: this.matchingPasswords('password', 'confirm') });
  }

  matchingPasswords(password, confirm) {
    return (group: FormGroup) => {
      if(group.controls[password].value === group.controls[confirm].value)
        return null;
      else
        return { 'matchingPasswords': true };
    };
  }

  onSubmit() {
    const user = {
      email: this.form.get('email').value,
      username: this.form.get('username').value,
      password: this.form.get('password').value
    };
    this.authService.register(user).subscribe(data => {
      if(!data.success) {
        this.messageClass = 'alert alert-danger';
      } else {
        this.messageClass = 'alert alert-success';
        setTimeout(() => {
          this.router.navigate(['/login']);
        }, 1000);
      }
      this.message = data.message;
    });
  }

}
