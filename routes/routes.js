const jwt = require('jsonwebtoken');

const AuthController = require('../controllers/auth_controller');
const HiController = require('../controllers/hi_controller');
const UserController = require('../controllers/user_controller');
const config = require('../config/database');

module.exports = (app) => {

  app.post('/register', AuthController.register);
  app.post('/login', AuthController.login);

  //JWT Middleware

  function isAuthenticated(req, res, next) {
    const token = req.headers['authorization'];
    jwt.verify(token, config.secret, (err, payload) => {
      if(err) {
        console.log(err);
        next();
      }
      else {
        req.payload = payload;
        next();
      }
    });
  };

  app.post('/hi', isAuthenticated, HiController.create);
  app.get('/hi', isAuthenticated, HiController.read);
  app.get('/users', isAuthenticated, UserController.read);

};
