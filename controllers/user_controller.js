const User = require('../models/user');

module.exports = {

  read(req, res) {
    const userId = req.payload.userId;
    User.find({ _id: { $ne: userId } })
    .then((users) => {
      res.send({ success: true, message: 'Users found.', users: users });
    })
    .catch((err) => {
      res.send({ success: false, message: 'Retrieving users failed.' });
    });
  }

}
