const Hi = require('../models/hi');
const User = require('../models/user');

module.exports = {

  create(req, res) {
    const userId = req.payload.userId;
    var body = req.body;
    User.findById(userId).select('email username')
    .then((user) => {
       if(user && body.username) {
         let hi = new Hi({
           email: user.email,
           from: user.username,
           to: body.username,
           date: Date.now()
         });

         hi.save()
         .then((hi) => {
           res.send({ success: true, message: 'Saved.' });
         })
         .catch((err) => {
           res.send({ success: false, message: 'Save failed.' });
         });
       } else {
           res.send({ success: false, message: 'User or To came back empty.' });
       }
    })
    .catch((err) => {
       res.send({ success: false, message: 'User not found.' });
    });
  },

  read(req, res) {
    const userId = req.payload.userId;
    User.findById(userId).select('username')
    .then((user) => {
      Hi.find({ to: user.username })
      .then((his) => {
        res.send({ success: true, message: 'His were found.', his: his });
      })
      .catch((err) => {
        res.send({ success: false, message: 'Retrieving his failed.' });
      });
    })
    .catch((err) => {
       res.send({ success: false, message: 'User not found.' });
    });
  }

}
