const jwt = require('jsonwebtoken');

const config = require('../config/database');
const User = require('../models/user');

module.exports = {

  register(req, res) {
    var body = req.body;
    if(body.email && body.username && body.password) {
      let user = new User({
        email: body.email,
        username: body.username,
        password: body.password
      });
      
      user.save()
      .then((user) => {
        res.send({ success: true, message: 'Account registered' });
      })
      .catch((err) => {
        if(err.code === 11000) {
          return res.send({ success: false, message: 'Username and/or email already exists' }); 
        }
        res.send({ success: false, message: 'Save failed.' });
      });
    } else {
      res.send({ success: false, message: 'Email and/or Username and/or Password not provided' });
    }
  },

  login(req, res) {
    var body = req.body;
    if(body.username && body.password) {
      User
      .findOne({ username: body.username })
      .then((user) => {
        if(user && user.comparePassword(body.password)) {
          const token = jwt.sign({ userId: user._id }, config.secret);
          res.send({ success: true, message: 'Success', token: token });
        } else {
          res.send({ success: false, message: 'Invalid username and/or password' });
        }
      })
      .catch((err) => {
        res.send({ success: false });
      });
    } else {
      res.send({ success: false, message: 'Username and/or password are required' });
    }
  }

}